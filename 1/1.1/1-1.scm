(define (abs x)
  (if (< x 0)
      (- x)
      x))

(define (new-if predicate then-clause else-clause)
    (cond (predicate then-clause)
          (else else-clause)))


(define (sqrt-iter guess x)
  (new-if (good-enough? guess x)
          guess
          (sqrt-iter (improve guess x)
                     x)))




(define (mystery a b)
  ((if (> b 0) + -) a b))

(define (myst2 a b)
  (if (> b 0) (+ a b) (- a b))
  )
