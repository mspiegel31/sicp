# The Elements of Programming

 - primitive expressions, the simplest entities a language is concerned with (i.e. `int`)
 - means of combination, by which compound elements are built from simpler ones
 - means of abstraction, by which compound elements can be named and manipulated as units
