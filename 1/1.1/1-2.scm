
(define (fact-iter product counter max-count)
  (if (> counter max-count)
      product ; true condition
      (fact-iter (* counter product) ; false condition
                 (+ counter 1)
                 max-count)))

(define (factorial n)
  (fact-iter 1 1 n))
