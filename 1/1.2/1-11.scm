;1.11
;A function f is defined by the rule that:
; f(n)=n if n < 3
; and
; f(n)=f(n−1)+2f(n−2)+3f(n−3) if n > 3

;Write a procedure that computes ff by means of a recursive process.



; recursive solution

(define (f n)
  (if (< n 3)
      n
      (+ (f (- n 1))
         (* 2 (f (- n 2)))
         (* 3 (f (- n 3))))
    )
  )

(define (f2 n)
  (f_iter 1 2 3 1 n)

(define (f_iter a b c counter)
  (if (< counter 3)
    ;magic
    (define new_a b)
    (define new_b c)
    (define new_c ?) ; magic

  (f_iter 0 1 2 n))
