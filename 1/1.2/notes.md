# Procedures and the Processes They Generate
---
Key Topic areas
  - Common "shapes" for processes _generated_ by simple procedures
  - Consumption of computing resources for these processes w.r.t. _time_ and _space_


Procedure: a pattern for the local evolution of a computational process, specifying how each stage of the process is built upon the previous stage.

- Procedures _define_ processes
- a procedure might represent a recursive defintion of the process, but the process itself doesn't have to be recursive.  it could be iterative in nature.
- Put another way, a procedure is the _blueprint_, the process is the _actual building_ (thanks [Stackoverflow](http://stackoverflow.com/questions/39320978/sicp-procedures-versus-process))

### Recursive process
A process characterized by chain of deferred operations that "build up" as the interpreter expands each call to the function, and "contract" as the results of each function are evaluated.

#### _Linear_ recursive process
A recursive whose operations and state grow linearly with the size of the input
- time = `O(n)`
- space = `O(n)`

##### Example
**procedure**
```
(define (factorial n)
  (if (= n 1)
      1
      (* n (factorial (- n 1)))))
```
**process**
```
(fact 4)
(* 4 (fact 3))
(* 4 (* 3 (fact 2)))
(* 4 (* 3 (* 2 fact 1)))
(* 4 (* 3 (* 2 1)))
(* 4 (* 3 2))
(* 4 (* 3 2))
(* 4 6)
24
```

### Iterative Process
A process whose state can be summarized by a **fixed number of state variables**, together with a fixed rule that describes how the state variables should be updated as the process moves from state to state.  An optional end test can specify the conditions under which said process can terminate

#### _Linear_ iterative process
An iterative process whose operations grow linearly with the size of the input
- time = `O(n)`
- space = `O(c)` (constant sized, remember, *fixed number of state variables*)

##### Example
**procedure**
```
(define (factorial n)
  (fact-iter 1 1 n))

(define (fact-iter product counter max-count)
  (if (> counter max-count)
      product
      (fact-iter (* counter product)
                 (+ counter 1)
                 max-count)))
```
**process**
```
(fact 4)
(fact-iter 1 1 4)
(fact-iter 1 2 4)
(fact-iter 2 3 4)
(fact-iter 6 4 4)
24
```
Examples from lecture
---
> "Recursion is all about enumerating the "easy cases" and then looking for ways to break the problem down into subproblems."

computing sqrt as an __iterative__ process
```
(define (sqrt x)
  (fixed point
    (f(y)(average (/ x y) y))
    1))

; iterative solution
(define (fixed-point f start)
  (define tolerance 0.00001)  ; tolerance variable
  (define (close-enuf? u v)   ; are u and v "close enough" given tolerance
    (< (abs (- u v)) tolerance)
  (define (iter old new)      ; looping construct
    (if (close-enuf? old new)
      new
      (iter new (f new))))
  (iter start (f start)))     ; initialize loop with starting values
```
